package br.com.KSI.mvc.KSICadastro.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.KSI.mvc.KSICadastro.controller.model.Cliente;
import br.com.KSI.mvc.KSICadastro.repository.ClienteRepository;
import jakarta.validation.Valid;

@Controller
@RequestMapping("clientes")
public class AtualizarController {
	@Autowired
	private ClienteRepository clienteRepository;

	@GetMapping("editarCliente")
	public String editarCliente(Cliente cliente) {
		return "clientes/editarCliente";
	}

	@PostMapping("editar")
	public String editarCliente(@Valid Cliente cliente, 
	  BindingResult result, Model model, Long id) {
		Optional<Cliente> clienteOptional = clienteRepository.findById(id);
		if(clienteOptional.isEmpty()) {
			return "clientes/editarCliente";
		}
	    if (result.hasErrors()) {
	    	return "clientes/editarCliente";
	    }
	         
		clienteRepository.save(cliente);

		return "redirect:/cadastro";
	}
	@ExceptionHandler(IllegalArgumentException.class)
	public String onError() {
		return "redirect:/cadastro";
	}

}
