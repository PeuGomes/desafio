package br.com.KSI.mvc.KSICadastro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KsiCadastroApplication {

	public static void main(String[] args) {
		SpringApplication.run(KsiCadastroApplication.class, args);
		
	}

}
