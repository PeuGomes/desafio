package br.com.KSI.mvc.KSICadastro.dto;

import java.time.LocalDate;

import br.com.KSI.mvc.KSICadastro.controller.model.Cliente;
import io.micrometer.common.lang.NonNull;

public class RequisicaoNovoCliente {
	private Long id;
	@NonNull
	private String nomeCliente;
	@NonNull
	private String cpfCliente;
	@NonNull
	private LocalDate dataNascimento;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNomeCliente() {
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	public String getCpfCliente() {
		return cpfCliente;
	}
	public void setCpfCliente(String cpfCliente) {
		this.cpfCliente = cpfCliente;
	}
	public LocalDate getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	public Cliente toCliente() {
		Cliente cliente = new Cliente();
		cliente.setNomeCliente(nomeCliente);
		cliente.setCpfCliente(cpfCliente);
		cliente.setDataNascimento(dataNascimento);
		return cliente;
	}
	
}
