package br.com.KSI.mvc.KSICadastro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.KSI.mvc.KSICadastro.controller.model.Cliente;
import br.com.KSI.mvc.KSICadastro.dto.RequisicaoNovoCliente;
import br.com.KSI.mvc.KSICadastro.repository.ClienteRepository;
import jakarta.validation.Valid;

@Controller
@RequestMapping("clientes")
public class CadastrarController {
	@Autowired
	private ClienteRepository clienteRepository;

	@GetMapping("formulario")
	public String formulario(RequisicaoNovoCliente requisicao) {
		return "clientes/formulario";
	}

	@PostMapping("novo")
	public String novo(@Valid RequisicaoNovoCliente requisicao, BindingResult result) {
		if (result.hasErrors()) {
			return "clientes/formulario";
		}
		Cliente cliente = requisicao.toCliente();
		clienteRepository.save(cliente);

		return "redirect:/cadastro";
	}
}
