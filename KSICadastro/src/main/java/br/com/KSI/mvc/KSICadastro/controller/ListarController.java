package br.com.KSI.mvc.KSICadastro.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.KSI.mvc.KSICadastro.controller.model.Cliente;
import br.com.KSI.mvc.KSICadastro.repository.ClienteRepository;

@Controller
@RequestMapping(path = "/cadastro")
public class ListarController {
	@Autowired
	private ClienteRepository clienteRepository;

	@GetMapping()
	public String cadastro(Model model) {
		List<Cliente> clientes = clienteRepository.findAll();
		model.addAttribute("clientes", clientes);
		return "clienteForm";
	}
	@ExceptionHandler(IllegalArgumentException.class)
	public String onError() {
		return "redirect:/cadastro";
	}
}
