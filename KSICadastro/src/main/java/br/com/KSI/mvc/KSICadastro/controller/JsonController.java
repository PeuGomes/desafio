package br.com.KSI.mvc.KSICadastro.controller;

import java.util.HashMap;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JsonController {
	
	@GetMapping("/json")
	public HashMap<String, String[]> cadastro() {
		HashMap<String, String[]> json = new HashMap<>();
		json.put("rejected_devices:",  new String[]{});
		json.put("accepted_devices:",  new String[]{"Google Pixel 2 XL (8.1.0)"});
		
		return json;
	}
	
}
